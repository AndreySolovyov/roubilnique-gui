//
//  RBQMainVC.h
//  Roubilnique-GUI
//
//  Created by Андрей on 19.04.15.
//  Copyright (c) 2015 Home. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class RBQProject;

@interface RBQMainVC : NSViewController<NSOutlineViewDataSource, NSOutlineViewDelegate>

@property (nonatomic, strong) RBQProject *project;

@property (nonatomic, strong) IBOutlet NSOutlineView *outlineView;

@property (nonatomic, strong) IBOutlet NSTableColumn *projectColumn;
@property (strong) IBOutlet NSTableColumn *inclusionStatusColumn;


@property (strong) IBOutlet NSButton *saveButton;
@property (strong) IBOutlet NSButton *restoreButton;

- (IBAction)saveEdits:(id)sender;
- (IBAction)restoreProject:(id)sender;


-(void)openProjectByPath:(NSString *)path;

@end
