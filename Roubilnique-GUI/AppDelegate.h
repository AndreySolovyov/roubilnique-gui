//
//  AppDelegate.h
//  Roubilnique-2.0
//
//  Created by Андрей on 19.04.15.
//  Copyright (c) 2015 Home. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>

- (IBAction)openFile:(id)sender;

@end

