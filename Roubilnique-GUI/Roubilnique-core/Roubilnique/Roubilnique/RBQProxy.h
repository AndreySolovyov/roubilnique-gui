//
//  RBQProxy.h
//  Roubilnique
//
//  Created by Андрей on 12.04.15.
//  Copyright (c) 2015 Home. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RBQProxy : NSObject

@property (nonatomic, strong) NSString *proxyRef;
@property (nonatomic, strong) NSDictionary *proxyStructure;

@end
