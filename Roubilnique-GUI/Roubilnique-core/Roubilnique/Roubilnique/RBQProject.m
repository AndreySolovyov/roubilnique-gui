//
//  RBQSubproject.m
//  Roubilnique
//
//  Created by Андрей on 11.04.15.
//  Copyright (c) 2015 Home. All rights reserved.
//

#import "RBQProject.h"

#import "XCProject.h"
#import "XCTarget.h"
#import "XCGroup.h"
#import "RBQChild.h"
#import "RBQProxy.h"

@interface RBQProject()
{
  XCProject *project;
  
  NSMutableDictionary *objects;
  
  NSMutableDictionary *objects_bak;
  RBQProject *project_bak;
  
  NSArray *projectReferences;
  NSMutableArray *containerItemProxies;
  
  NSMutableArray *frameworksBuildPhaseFiles;
  NSMutableArray *resourcesBuildPhaseFiles;
  
  NSMutableDictionary *pbxBuildFilesToRefs;
}

  //@property (nonatomic, strong, readwrite) NSString *path;

  //@property (nonatomic, getter=isIncludedInBuild, readwrite) BOOL includedInBuild;

@end


@implementation RBQProject

-(instancetype)init{
  
  self = [super init];
  
  if(self){
    self.subprojects = [NSMutableArray array];
    
    projectReferences = [NSArray array];
    containerItemProxies = [NSMutableArray array];
    pbxBuildFilesToRefs = [NSMutableDictionary dictionary];
    
    _includedInBuild = NO;
  }
  
  return self;
}

-(instancetype)initWithPath:(NSString *)path
{
  self = [self init];
  
  if(self){
    self.path = path;
    
    [self analyze];
  }
  
  return self;
}

-(void)analyze
{
  NSLog(@"-= XCODEPROJ ANALYZING =-");
  
  project = [[XCProject alloc] initWithFilePath:self.path];
  
  objects = project.objects;
  
  objects_bak = [self backupObjects:objects];
  
  [self fillReferences];
  [self fillSubprojects];
  
    //project_bak = [self backupObjects:project];
}

-(void)fillReferences
{
  for(NSString *key in objects.allKeys){
    NSMutableDictionary *structure = objects[key];
    NSArray *allKeys = [structure allKeys];
    
    if([allKeys containsObject:@"projectReferences"]){
      NSLog(@"Мы нашли их гнездо!");
      
      projectReferences = structure[@"projectReferences"];
      continue;
    }
    
    if([allKeys containsObject:@"isa"]){
      NSString *fileType = structure[@"isa"];
      
      if([fileType isEqualToString:@"PBXContainerItemProxy"]){
        [containerItemProxies addObject:@{@"ref" : key, @"proxy" : structure}];
      }
      else if([fileType isEqualToString:@"PBXFrameworksBuildPhase"]){
        frameworksBuildPhaseFiles = structure[@"files"];
      }
      else if([fileType isEqualToString:@"PBXResourcesBuildPhase"]){
        resourcesBuildPhaseFiles = structure[@"files"];
      }
      else if([fileType isEqualToString:@"PBXBuildFile"]){
        NSString *childRef = structure[@"fileRef"];
        [pbxBuildFilesToRefs setObject:key forKey:childRef ? childRef : @""];
      }
    }
  }
}

-(void)fillSubprojects
{
  for(NSString *key in objects.allKeys){
    
    NSMutableDictionary *structure = objects[key];
    NSArray *allKeys = [structure allKeys];
    
    if([allKeys containsObject:@"lastKnownFileType"]){
      NSString *fileType = structure[@"lastKnownFileType"];
      
      if([fileType isEqualToString:@"wrapper.pb-project"]){
        RBQProject *subproject = [RBQProject new];
        
        subproject.path = structure[@"path"];
        
        subproject.ProjectRef = key;
        subproject.ProductGroup = [self productGroupForRef:subproject.ProjectRef];
        subproject.children = [self childrenForProductGroup:subproject.ProductGroup];
        subproject.proxies = [self proxiesForSubprojectRef:subproject.ProjectRef];
        
        subproject.includedInBuild = [self isSubprojectIncludedInBuild:subproject];
        
        [self.subprojects addObject:subproject];
      }
    }
  }
}

-(NSString *)productGroupForRef:(NSString *)ref
{
  for(NSDictionary *projectReference in projectReferences){
    
    NSString *projectRef = projectReference[@"ProjectRef"];
    
    if([projectRef isEqualToString:ref]){
      return projectReference[@"ProductGroup"];
    }
  }
  
  return nil;
}

-(NSArray *)proxiesForSubprojectRef:(NSString *)ref
{
  NSMutableArray *proxies = [NSMutableArray array];
  
  for(NSString *key in objects.allKeys){
    
    NSDictionary *structure = objects[key];
    NSArray *allKeys = [structure allKeys];
    
    if([allKeys containsObject:@"containerPortal"]){
      NSString *portalRef = structure[@"containerPortal"];
      
      if([portalRef isEqualToString:ref]){
        RBQProxy *proxy = [RBQProxy new];
        proxy.proxyRef = key;
        proxy.proxyStructure = structure;
        
        [proxies addObject:proxy];
      }
    }
  }
  
  return proxies;
}

-(NSArray *)childrenForProductGroup:(NSString *)productGroup
{
  XCGroup *group = [project groupWithKey:productGroup];
  NSArray *children = group.children;
  
  NSMutableArray *childrenForProductGroup = [NSMutableArray array];
  
  for(NSString *childRef in children){
    NSString *fileRef = pbxBuildFilesToRefs[childRef];
    
    RBQChild *child = [RBQChild new];
    child.childRef = childRef;
    child.fileRef = fileRef;
    child.childStructure = [objects objectForKey:child.childRef];
    
    if([frameworksBuildPhaseFiles containsObject:child.fileRef]){
      child.type = RBQChildTypeFramework;
      
    } else if([resourcesBuildPhaseFiles containsObject:child.fileRef]){
      child.type = RBQChildTypeResource;
    }
    
    [childrenForProductGroup addObject:child];
  }
  
  return childrenForProductGroup;
}

-(BOOL)removeSubprojectsFromBuild:(NSArray *)subprojectsToRemove
{
  for(RBQProject *subproject in subprojectsToRemove){
    
    for(RBQChild *child in  subproject.children){
      [objects removeObjectForKey:child.childRef];
      
      switch(child.type){
        case RBQChildTypeResource:
          [resourcesBuildPhaseFiles removeObject:child.fileRef];
          break;
          
        case RBQChildTypeFramework:
          [frameworksBuildPhaseFiles removeObject:child.fileRef];
          break;
          
        default:
          break;
      }
    }
    
    for(RBQProxy *proxy in subproject.proxies){
      [objects removeObjectForKey:proxy.proxyRef];
    }
    
    subproject.includedInBuild = NO;
  }
  
  return YES;
}

-(BOOL)addSubprojectsToBuild:(NSArray *)subprojects
{
  for(RBQProject *subproject in subprojects){
    
    for(RBQChild *child in subproject.children){
      NSObject *object = child.childStructure;
      
      if(!object){
        NSLog(@"ERROR INSERTING CHILD STRUCTURE FOR %@", subproject.path);
        continue;
      }
      
      [objects setObject:child.childStructure forKey:child.childRef];
      
      switch(child.type){
        case RBQChildTypeResource:
          [resourcesBuildPhaseFiles addObject:child.fileRef];
          break;
          
        case RBQChildTypeFramework:
          [frameworksBuildPhaseFiles addObject:child.fileRef];
          break;
          
        default:
          break;
      }
    }
    
    for(RBQProxy *proxy in subproject.proxies){
      [objects setObject:proxy.proxyStructure forKey:proxy.proxyRef];
    }
    
    subproject.includedInBuild = YES;
  }
  return NO;
}

-(void)save
{
  [project save];
}

-(void)restore
{
  [project.objects removeAllObjects];
  [project.objects addEntriesFromDictionary:objects_bak];
  
  [project save];
}

-(NSString *)description
{
  NSMutableString *desc = [NSMutableString stringWithString:[NSString stringWithFormat:@"\nRBQProject: %@\nChildren:\n", self.path]];
  
  for(RBQChild *child in self.children){
    [desc appendString:[NSString stringWithFormat:@"%@ -- %@\n", child.childRef, child.fileRef]];
  }
  [desc appendString:@"\nProxies:\n"];
  
  for(RBQProxy *proxy in self.proxies){
   [desc appendString:[NSString stringWithFormat:@"%@\n", proxy.proxyRef]];
  }
  
  [desc appendString:@"\n\n"];
  
  
  return desc;
}

-(id)backupObjects:(id)source
{
  id backup;
  
    // Deep copy "all" objects in _dict1 pointers and all to _dict2
  NSData *buffer = [NSKeyedArchiver archivedDataWithRootObject:source];
  backup = [NSKeyedUnarchiver unarchiveObjectWithData:buffer];
  
  return backup;
}

-(BOOL)isSubprojectIncludedInBuild:(RBQProject *)subproject
{
  if(subproject.proxies.count){
    return YES;
  }
  
  for(RBQChild *child in subproject.children){
    if(child.type != RBQChildTypeUnknown){
      return YES;
    }
  }
  
  return NO;
  
//    BOOL included = NO;
//    
//    for(RBQChild *child in subproject.children){
//      
//      switch(child.type){
//        case RBQChildTypeResource:
//          included = [resourcesBuildPhaseFiles containsObject:child.fileRef];
//          break;
//          
//        case RBQChildTypeFramework:
//          included = [frameworksBuildPhaseFiles containsObject:child.fileRef];
//          break;
//          
//        default:
//          break;
//      }
//    }
//    
//    if(!included){
//      for(NSString *proxy in subproject.proxies){
//        
//        included |= [objects.allKeys containsObject:proxy];
//        
//        if(included) break;
//      }
//    }
//
//  
//  return included;
}

@end
