//
//  RBQChild.h
//  Roubilnique
//
//  Created by Андрей on 12.04.15.
//  Copyright (c) 2015 Home. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, RBQChildType)
{
  RBQChildTypeUnknown,
  RBQChildTypeResource,
  RBQChildTypeFramework
};


@interface RBQChild : NSObject

@property (nonatomic, strong) NSString *childRef;
@property (nonatomic, strong) NSString *fileRef;

@property (nonatomic, strong) NSDictionary *childStructure;
@property (nonatomic) RBQChildType type;

@end
