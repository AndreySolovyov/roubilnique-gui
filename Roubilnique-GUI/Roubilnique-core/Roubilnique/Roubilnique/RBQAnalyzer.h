//
//  RBQAnalyzer.h
//  Roubilnique
//
//  Created by Андрей on 05.04.15.
//  Copyright (c) 2015 Home. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RBQProject.h"

@interface RBQAnalyzer : NSObject
-(void)analyze;

@property (nonatomic, strong) RBQProject *project;

@end
