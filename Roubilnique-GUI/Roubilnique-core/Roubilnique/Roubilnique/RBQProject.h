//
//  RBQSubproject.h
//  Roubilnique
//
//  Created by Андрей on 11.04.15.
//  Copyright (c) 2015 Home. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RBQProject : NSObject

@property (nonatomic, strong) NSString *ProjectRef;
@property (nonatomic, strong) NSString *ProductGroup;

@property (nonatomic, strong) NSString *path;

@property (nonatomic, strong) NSArray *children;
@property (nonatomic, strong) NSArray *proxies;

@property (nonatomic, strong) NSMutableArray *subprojects;

@property (nonatomic, getter=isIncludedInBuild) BOOL includedInBuild;


-(instancetype)initWithPath:(NSString *)path;

-(BOOL)removeSubprojectsFromBuild:(NSArray *)subprojects;
-(BOOL)addSubprojectsToBuild:(NSArray *)subprojects;

-(void)save;
-(void)restore;

@end
