//
//  AppDelegate.m
//  Roubilnique-2.0
//
//  Created by Андрей on 19.04.15.
//  Copyright (c) 2015 Home. All rights reserved.
//

#import "AppDelegate.h"
#import "Roubilnique-core/Roubilnique/Roubilnique/RBQAnalyzer.h"
#import "RBQMainVC.h"

@interface AppDelegate ()

@property (weak) IBOutlet NSWindow *window;

@property (nonatomic, strong) RBQMainVC *mainVC;

@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
  
  [self.window.contentView setAutoresizesSubviews:YES];
  
    // 1. Create the master View Controller
  self.mainVC = [[RBQMainVC alloc] initWithNibName:@"RBQMainVC" bundle:nil];
  
    // 2. Add the view controller to the Window's content view
  [self.window.contentView addSubview:self.mainVC.view];
  self.mainVC.view.frame = ((NSView*)self.window.contentView).bounds;
}

- (void)applicationWillTerminate:(NSNotification *)aNotification {
  // Insert code here to tear down your application
}

- (IBAction)openFile:(id)sender {
  
  NSOpenPanel *panel = [NSOpenPanel openPanel];
  panel.title = @"Choose an XCode project";
  panel.allowsMultipleSelection = NO;
  panel.allowedFileTypes = @[@"xcodeproj"];
    //panel.canChooseDirectories = YES;
  
  [panel beginSheetModalForWindow:self.window
                completionHandler:^(NSInteger result) {
                  
                  if (result==NSOKButton) {
                    
                    NSURL *selection = panel.URLs[0];
                    NSString* path = [selection.path stringByResolvingSymlinksInPath];
                    
                    NSLog(@"Roubilnique-GUI: opened file: %@", path);
                    [self.mainVC openProjectByPath:path];
                  }
                  
                }];
}

@end
