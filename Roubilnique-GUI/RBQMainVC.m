//
//  RBQMainVC.m
//  Roubilnique-GUI
//
//  Created by Андрей on 19.04.15.
//  Copyright (c) 2015 Home. All rights reserved.
//

#import "RBQMainVC.h"
#import "Roubilnique-core/Roubilnique/Roubilnique/RBQProject.h"
#import "Roubilnique-core/Roubilnique/Roubilnique/RBQChild.h"

#import <objc/runtime.h>

@interface RBQMainVC ()
{
  NSUserDefaults *defaults;
  NSString *lastProjectPath;
}

@end

@implementation RBQMainVC

-(void)loadView
{
  [super loadView];
  
  self.outlineView.delegate = self;
  self.outlineView.dataSource = self;
  
  defaults = [NSUserDefaults standardUserDefaults];
  
  lastProjectPath = [defaults objectForKey:@"lastProjectPath"];
  
  if(lastProjectPath){
    [self openProjectByPath:lastProjectPath];
  }
}

-(void)openProjectByPath:(NSString *)path
{
  [self backupProjectFile:path];
  
  self.project = [[RBQProject alloc] initWithPath:path];
  [defaults setObject:path forKey:@"lastProjectPath"];
  
  [self updateInterface];
}

-(void)backupProjectFile:(NSString *)path
{
  
}

-(void)updateInterface
{
  [self.outlineView reloadData];
  self.view.window.title = self.project.path;
}


-(id)outlineView:(NSOutlineView *)outlineView child:(NSInteger)index ofItem:(id)item
{
  id res = nil;
  
  if(item == nil){
    
    res = self.project.subprojects[index];
    
  } else {
    if([self isItemProject:item]){
      RBQProject *project = (RBQProject *)item;
      
      if(index == 0){
        res = project.children;
      } else if(index == 1){
        res = project.subprojects;
      }
    } else if([self isItemChild:item]){
      RBQChild *child = (RBQChild*)item;
      res = child.childStructure[@"path"];
    }
      else if([item isKindOfClass:[NSArray class]]){
      NSArray *subitems = (NSArray *)item;
      if(subitems.count){
        res = subitems[index];
      }
    }
  }
  
  return res;
}

-(BOOL)outlineView:(NSOutlineView *)outlineView isItemExpandable:(id)item
{
  BOOL expandable = NO;
  
  if([self isItemProject:item]){
    RBQProject *project = (RBQProject *)item;
    
    expandable = project.children.count != 0;
    expandable |= project.subprojects.count != 0;
    
  } else if([self isItemArray:item]){
    return [item count];
  }
  
  return expandable;
}


-(NSInteger)outlineView:(NSOutlineView *)outlineView numberOfChildrenOfItem:(id)item
{
  NSInteger number = 0;
  
  if(item == nil){
    number = self.project.subprojects.count;
  }
  else {
    if([self isItemProject:item]){
      RBQProject *project = (RBQProject *)item;
      
      number = 0;
      
      if(project.subprojects.count){
        number++;
      }
      if(project.children.count){
        number++;
      }
    } else if([item isKindOfClass:[NSArray class]]){
      number = [((NSArray *)item) count];
    } else if([self isItemArray:item]){
      number = [item count];
    }
  }
  
  return number;
}

-(id)outlineView:(NSOutlineView *)outlineView objectValueForTableColumn:(NSTableColumn *)tableColumn byItem:(id)item
{
  id res = nil;
  
  if(tableColumn == self.projectColumn){
    if([self isItemProject:item]){
      
      RBQProject *project = (RBQProject *)item;
      res = project.path;
      
    } else if([item isKindOfClass:[NSString class]]){
      res = item;
    } else if([self isItemChild:item]){
      RBQChild *child = (RBQChild*)item;
      res = child.childStructure[@"path"];
    } else if([self isItemArray:item]){
      id subitem = item[0];
      
      if([self isItemChild:subitem]){
        res = @"Targets";
      }
      else if([self isItemProject:subitem]){
        res = @"Subprojects";
      }
    }
  }
  
  return res;
}


//-(BOOL)isItemFolder:(id)item
//{
//  return [self doesItem:item passIsaComparison:@"folder"];
//}

-(BOOL)isItemProject:(id)item
{
  return [item isKindOfClass:[RBQProject class]];
}

-(BOOL)isItemChild:(id)item
{
  return [item isKindOfClass:[RBQChild class]];
}

-(BOOL)isItemArray:(id)item
{
  return [item isKindOfClass:[NSArray class]];
}



- (NSView *)outlineView:(NSOutlineView *)outlineView viewForTableColumn:(NSTableColumn *)tableColumn item:(id)item NS_AVAILABLE_MAC(10_7)
{
  NSView *view = nil;
  
  if(tableColumn == self.inclusionStatusColumn){
    if([self isItemProject:item]){
      RBQProject *project = (RBQProject *)item;
      
      NSButton *inclusionCheckBox = [[NSButton alloc] init];
      
      inclusionCheckBox.target = self;
      
      [inclusionCheckBox setButtonType:NSSwitchButton];
      [inclusionCheckBox setTitle:@""];
      
      if([self isItemProject:item]){
        
        inclusionCheckBox.state = project.isIncludedInBuild ? NSOnState : NSOffState;
        objc_setAssociatedObject(inclusionCheckBox, @"project", item, OBJC_ASSOCIATION_RETAIN);
      }
      /*
      if([self isItemFolder:item]){
        [inclusionCheckBox setAction:@selector(handleFolderSelection:)];
      } else */
      if([self isItemProject:item]){
        [inclusionCheckBox setAction:@selector(handleProjectSelection:)];
      } else if([self isItemChild:item]){
        [inclusionCheckBox setAction:@selector(handleTargetSelection:)];
      }
      
      view = inclusionCheckBox;
    }
  } else {
    NSTextField *label = [[NSTextField alloc] init];
    
    [label setBezeled:NO];
    [label setDrawsBackground:NO];
    [label setEditable:NO];
    [label setSelectable:NO];
    
    view = label;
  }
  
  return view;
}

-(void)handleFolderSelection:(id)sender
{
  NSLog(@"handleFolderSelection:");
}

-(void)handleProjectSelection:(id)sender
{
  NSLog(@"handleProjectSelection:");
  RBQProject *project = objc_getAssociatedObject(sender, @"project");
  
  if(project){
    BOOL shouldExclude = [sender state] == NSOffState;
    
    if(shouldExclude){
      [self.project removeSubprojectsFromBuild:@[project]];
    } else {
      [self.project addSubprojectsToBuild:@[project]];
    }
    
    [self updateInterface];
  }
}

-(void)handleTargetSelection:(id)sender
{
  NSLog(@"handleTargetSelection:");
}

- (IBAction)saveEdits:(id)sender {
  [self.project save];
  [self updateInterface];
}

- (IBAction)restoreProject:(id)sender {
  [self.project restore];
  
  [self openProjectByPath:lastProjectPath];
  
  [self updateInterface];
}

@end
